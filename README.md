# Quadro Societário - Desafio
## Apresentação
Este projeto se concentra no desenvolvimento de uma aplicação em PHP baseada no framework Symfony, visando a implementação de um sistema empresarial fictício. Inicialmente, foram desenvolvidas apenas duas entidades com suas funcionalidades básicas, seguindo uma abordagem simplista.

### Instruções para execução
#### Pré-requisitos
Para a elaboração do projeto foi utilizado uma máquina com Processador AMD Ryzen 5, 8 GB RAM, 128 GB de armazenamento e sistema operacional Windows 11. 

As tecnologias utilizadas foram:
- PHP na versão 8.2.12 
- Composer version 2.7.1 
- Symfony CLI version 5.8.11
- PostgreSQL 16

#### Executando o projeto
**Para iniciar o projeto siga os sequintes passos**
```
git clone https://github.com/WandeilsonFerreira/Quadro-societario
cd quadro-societario
composer install
```

O Synfony não possui banco de dados nativo, mas fornece todas as ferramentas necessárias para utiliza-los em suas aplicações graças ao Doctrine.

Instale as seguintes dependências: 
```
composer require symfony/orm-pack
composer require --dev symfony/maker-bundle
```

Configure a conecção com o DB editando o arquivo .env na raiz do projeto. 
```
DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=16&charset=utf8"

```

> Caso ache necessário, o arquivo .env pode ser sobrescrito criando o arquivo .env.local evitando assim compartilhar suas credenciais. 

**Criando a base de dados**
```
php bin/console doctrine:database:create
```

Executando a migração das entidades
    
Uma vez configurado o banco de dados podemos fazer as migrações das bases
```
symfony console meke:migration
```

**Testando a aplicação**

Para executar o projeto temos que iniciar um servidor local utilizando o seguinte comando: 
```
symfone serve
```
Uma vez iniciado podemos acessar o endereço fornecido pelo framework (http://127.0.0.1:8000) e teremos acesso a página inicial do Symfony. 


Para testarmos a aplicação podemos analisar o CRUD diretamente pelo terminal e acessá-lo pelas rotas de Empresa e Socio. 

## Empresa

**Read**

http://127.0.0.1:8000/admin/empresa/
<center>

![](img/1.png)

</center>

**Create**

http://127.0.0.1:8000/admin/empresa/store/

<center>

![](img/2.png)

</center>

**Update**

http://127.0.0.1:8000/admin/empresa/update/

<center>

![](img/3.png)

</center>


**Delete**

http://127.0.0.1:8000/admin/empresa/remove/


<center>

![](img/4.png)

</center>

## Sócio


**Read**

http://127.0.0.1:8000/admin/socio/
<center>

![](img/5.png)

</center>

**Create**

http://127.0.0.1:8000/admin/socio/store/

<center>

![](img/6.png)

</center>

**Update**

http://127.0.0.1:8000/admin/socio/update/

<center>

![](img/7.png)

</center>


**Delete**

http://127.0.0.1:8000/admin/socio/remove/


<center>

![](img/4.png)

</center>



<center>

**Wandeilson Ferreira**

</center>