<?php

namespace App\Entity;

use App\Repository\SocioRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SocioRepository::class)]
#[ORM\Table(name: 'socio')]

class Socio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nome = null;

    #[ORM\Column(length: 255)]
    private ?string $endereco = null;

    #[ORM\Column(length: 12)]
    private ?string $teledone = null;

    #[ORM\Column(length: 255)]
    private ?string $razaosocial = null;

    #[ORM\Column(length: 255)]
    private ?string $observacoes = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): static
    {
        $this->nome = $nome;

        return $this;
    }

    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(string $endereco): static
    {
        $this->endereco = $endereco;

        return $this;
    }

    public function getTeledone(): ?string
    {
        return $this->teledone;
    }

    public function setTeledone(string $teledone): static
    {
        $this->teledone = $teledone;

        return $this;
    }

    public function getRazaosocial(): ?string
    {
        return $this->razaosocial;
    }

    public function setRazaosocial(string $razaosocial): static
    {
        $this->razaosocial = $razaosocial;

        return $this;
    }

    public function getObservacoes(): ?string
    {
        return $this->observacoes;
    }

    public function setObservacoes(string $observacoes): static
    {
        $this->observacoes = $observacoes;

        return $this;
    }
}
