<?php

namespace App\Controller\Admin;

use App\Entity\Empresa;
use App\Repository\EmpresaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/empresa', name: 'admin_empresa_', methods: ['GET'])]
class EmpresaController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('admin/empresa/index.html.twig', [
            'controller_name' => 'EmpresaController',
        ]);
    }

    #[Route('/store', name: 'store', methods: ['POST'])]
    public function store(EntityManagerInterface $manager): Response
    {
        $empresa = new Empresa();
        $empresa->setRazaosocial('Maria e Eduarda SA');
        $empresa->setNomefantasia('Toc MIX');
        $empresa->setEndereco('Rua 11');
        $empresa->setCidade('Patos');
        $empresa->setEstado('Paraíba');

        $manager->persist($empresa);
        $manager->flush();

        return new Response('Empresa cadastrada com sucesso!');
    }

    #[Route('/update', name: 'update', methods: ['POST'])]
    public function update(EntityManagerInterface $manager, EmpresaRepository $empresaRepository): Response
    {
        $empresa = $empresaRepository->find(2);
        $empresa->setEndereco('Rua 13 de maio 954');
        
        $manager->flush();
        
        return new Response('Dado da empresa atualizada com sucesso!');
    }

    #[Route('/remove', name: 'remove', methods: ['POST'])]
    public function remove(EntityManagerInterface $manager, EmpresaRepository $empresaRepository): Response
    {
        $empresa = $empresaRepository->find(2);
        $manager->remove($empresa);

        $manager->flush();

        return new Response('Dado da empresa removido com sucesso!');
    }
}
