<?php

namespace App\Controller\Admin;
use App\Entity\Socio;
use App\Repository\EmpresaRepository;
use App\Repository\SocioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/socio', name: 'admin_socio_', methods: ['GET'])]
class SocioController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('admin/socio/index.html.twig', [
            'controller_name' => 'SocioController',
        ]);
    }

    #[Route('/store', name: 'store', methods: ['POST'])]
    public function store(EntityManagerInterface $manager): Response
    {
        $socio = new Socio();
        $socio->setNome('MAria do Carmo');
        $socio->setEndereco('Rua Maria Angela');
        $socio->setTeledone('(83)99808795');
        $socio->setRazaosocial('Armazem Paraíba');
        $socio->setObservacoes('Loja central');

        $manager->persist($socio);
        $manager->flush();

        return new Response('Socio cadastrado com sucesso!');
    }

    #[Route('/update', name: 'update', methods: ['POST'])]
    public function update(EntityManagerInterface $manager, SocioRepository $socioRepository): Response
    {
        $socio = $socioRepository->find(2);
        $socio->setNome('Maria do Carno da Luz');

        $manager->flush();

        return new Response('Dado do Socio atualizado com sucesso!');
    }

    #[Route('/remove', name: 'remove', methods: ['POST'])]
    public function remove(EntityManagerInterface $manager, SocioRepository $socioRepository): Response
    {
        $socio = $socioRepository->find(2);
        $manager->remove($socio);

        $manager->flush();

        return new Response('Dado do socio removido com sucesso!');
    }
}